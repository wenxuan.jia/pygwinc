# GWINC CE2 (Silica) interferometer parameters
#
# References:
# 1. Electro-Optic Handbook, Waynant & Ediger (McGraw-Hill: 1993)
# 2. LIGO/GEO data/experience
# 3. Suspension reference design, LIGO-T000012-00
# 4. Quartz Glass for Optics Data and Properties, Heraeus data sheet,
#    numbers for suprasil
# 5. Y.S. Touloukian (ed), Thermophysical Properties of Matter
#    (IFI/Plenum,1970)
# 6. Marvin J. Weber (ed) CRC Handbook of laser science and technology,
#    Vol 4, Pt 2
# 7. R.S. Krishnan et al.,Thermal Expansion of Crystals, Pergamon Press
# 8. P. Klocek, Handbook of infrared and optical materials, Marcel Decker,
#    1991
# 9. Rai Weiss, electronic log from 5/10/2006
# 10. Wikipedia online encyclopedia, 2006
# 11. D.K. Davies, The Generation and Dissipation of Static Charge on
# dielectrics in a Vacuum, page 29
# 12. Gretarsson & Harry, Gretarsson thesis
# 13. Fejer
# 14. Braginsky


Infrastructure:
  Length: 40000                   # m; whoa
  Temp: 293                       # K
  ResidualGas:
    # polarizabilities from https://cccbdb.nist.gov/pollistx.asp
    H2:
      BeamtubePressure: 4.4e-8      # Pa
      ChamberPressure: 4.1e-7       # Pa
      mass: 3.35e-27                # kg; Mass of H_2 (ref. 10)
      polarizability: 7.87e-31      # m^3;

    N2:
      BeamtubePressure: 2.5e-9
      ChamberPressure: 1.1e-7
      mass: 4.65e-26
      polarizability: 1.71e-30

    H2O:
      BeamtubePressure: 4.0e-9
      ChamberPressure: 1.4e-7
      mass: 2.99e-26
      polarizability: 1.50e-30

    O2:
      BeamtubePressure: 2.8e-9
      ChamberPressure: 1.0e-7
      mass: 5.31e-26
      polarizability: 1.56e-30

  BuildingRadius: 10              # m


TCS:
  ## Parameter describing thermal lensing
  # The presumably dominant effect of a thermal lens in the ITMs is an increased
  # mode mismatch into the SRC, and thus an increased effective loss of the SRC.
  # The increase is estimated by calculating the round-trip loss S in the SRC as
  # 1-S = |<Psi|exp(i*phi)|Psi>|^2, where
  # |Psi> is the beam hitting the ITM and
  # phi = P_coat*phi_coat + P_subs*phi_subs
  # with phi_coat & phi_subs the specific lensing profiles
  # and P_coat & P_subst the power absorbed in coating and substrate
  #
  # This expression can be expanded to 2nd order and is given by
  # S= s_cc P_coat^2 + 2*s_cs*P_coat*P_subst + s_ss*P_subst^2
  # s_cc, s_cs and s_ss were calculated analytically by Phil Willems (4/2007)
  s_cc: 7.024                     # Watt^-2
  s_cs: 7.321                     # Watt^-2
  s_ss: 7.631                     # Watt^-2
  # The hardest part to model is how efficient the TCS system is in
  # compensating this loss. Thus as a simple Ansatz we define the
  # TCS efficiency TCSeff as the reduction in effective power that produces
  # a phase distortion. E.g. TCSeff=0.99 means that the compensated distortion
  # of 1 Watt absorbed is eqivalent to the uncompensated distortion of 10mWatt.
  # The above formula thus becomes:
  # S= s_cc P_coat^2 + 2*s_cs*P_coat*P_subst + s_ss*P_subst^2 * (1-TCSeff)^2
  #
  # To avoid iterative calculation we define TCS.SCRloss = S as an input
  # and calculate TCSeff as an output.
  # TCS.SRCloss is incorporated as an additional loss in the SRC
  SRCloss: 0.00


Seismic:
  Site: 'LHO'                       # LHO or LLO (only used for Newtonian noise)
  KneeFrequency: 5                  # Hz; freq where 'flat' noise rolls off
  LowFrequencyLevel: 1e-9           # m/rtHz; seismic noise level below f_knee
  KneeFrequencyHorizontal: 4        # Hz; freq where 'flat' noise rolls off
  LowFrequencyLevelHorizontal: 1e-9 # m/rtHz; seismic noise level below f_knee
  Gamma: 0.8                        # abruptness of change at f_knee
  Rho: 1.8e3                        # kg/m^3; density of the ground nearby
  Beta: 0.8                         # quiet times beta: 0.35-0.60
                                    # noisy times beta: 0.15-1.4
  Omicron: 10                       # Feedforward cancellation factor
  TestMassHeight: 1.5               # m
  pWaveSpeed: 600                   # m/s
  sWaveSpeed: 300                   # m/s
  RayleighWaveSpeed: 250            # m/s
  pWaveLevel: 15                    # Multiple of the Peterson NLNM amplitude
  sWaveLevel: 15                    # Multiple of the Peterson NLNM amplitude
  PlatformMotion: '6D'


Atmospheric:
  AirPressure: 101325               # Pa
  AirDensity: 1.225                 # kg/m**3
  AirKinematicViscosity: 1.8e-5     # m**2/s
  AdiabaticIndex: 1.4               #
  SoundSpeed: 344                   # m/s
  WindSpeed: 10                     # m/s; typical value
  Temperature: 300                  # K
  TempStructConst: 0.2              # K**2/m**(2/3);
  TempStructExp: 0.667              #
  TurbOuterScale: 100               # m
  # TurbEnergyDissRate: 0.01        # m**2/s**3
  KolmEnergy1m: 1                   # Kolmogorov energy spectrum at 1/m [m**2/s**2]


Suspension:
  FiberType: 'Tapered'
  BreakStress: 750e6              # Pa; ref. K. Strain
  Temp: 290
  VHCoupling:
    theta: 3.1e-3 # vertical-horizontal x-coupling

  Fiber:
    Radius: 456e-6                # m
    # for tapered fibers
    # EndRadius is tuned to cancel thermo-elastic noise (delta_h in suspQuad)
    # EndLength is tuned to match bounce mode frequency
    EndRadius: 1.163e-3           # m
    EndLength: 45e-3              # m

  # Note stage numbering: mirror is at beginning of stack, not end
  Stage:
    # Stage1
    - Mass: 320                   # kg
      Length: 2                   # m
      Dilution: .nan              #
      K: 1.57e4                   # N/m; vertical spring constant
      WireRadius: .nan            # m
      Blade: 0.0045               # blade thickness
      WireMaterial: 'Silica'
      BladeMaterial: 'Silica'
      NWires: 4

    # Stage2
    - Mass: 320
      Length: 1.554
      Dilution: .nan
      K: 2.14e4
      WireRadius: 845e-6
      Blade: 13.3e-3
      NWires: 4
      WireMaterial: 'C70Steel'
      BladeMaterial: 'MaragingSteel'

    # Stage3
    - Mass: 299.2
      Length: 0.238
      Dilution: .nan
      K: 1.98e4
      WireRadius: 1.02e-3
      Blade: 15.7e-3
      NWires: 4
      WireMaterial: 'C70Steel'
      BladeMaterial: 'MaragingSteel'

    # Stage4
    - Mass: 560.8
      Length: 0.208
      Dilution: .nan  # 87
      K: 2.59e4
      WireRadius: 1.83e-3
      Blade: 16.8e-3
      NWires: 2
      WireMaterial: 'C70Steel'
      BladeMaterial: 'MaragingSteel'

  # Suspension material properties
  Silica:
    Rho: 2200.0         # Kg/m^3
    C: 772.0            # J/Kg/K
    K: 1.38             # W/m/kg
    Alpha: 3.9e-7       # 1/K
    dlnEdT: 1.52e-4     # (1/K), dlnE/dT
    Phi: 4.1e-10        # from G Harry e-mail to NAR 27April06
    Y: 72e9             # Pa; Youngs Modulus
    Dissdepth: 1.5e-2   # from G Harry e-mail to NAR 27April06

  C70Steel:
    Rho: 7800.0
    C: 486.0
    K: 49.0
    Alpha: 12e-6
    dlnEdT: -2.5e-4
    Phi: 2e-4
    Y: 212e9            # measured by MB for one set of wires

  MaragingSteel:
    Rho: 7800.0
    C: 460.0
    K: 20.0
    Alpha: 11e-6
    dlnEdT: 0.0
    Phi: 1.0e-4
    Y: 187e9
    # consistent with measured blade spring constants NAR


## Optic Material
Materials:
  MassRadius: 0.35                # m
  MassThickness: 0.378            # m

  ## Dielectric coating material parameters
  Coating:
    ## high index material: tantala
    Yhighn: 124e9                 # LMA (Granata at LVC) 2017 (was 140)
    Sigmahighn: 0.28              # LMA (Granata at LVC) 2017 (was 0.23)
    CVhighn: 2.1e6                # Crooks et al, Fejer et al
    Alphahighn: 3.6e-6            # 3.6e-6 Fejer et al, 5e-6 from Braginsky
    Betahighn: 1.4e-5             # dn/dT, value Gretarrson (G070161)
    ThermalDiffusivityhighn: 33   # Fejer et al
    Indexhighn: 2.06539
    Phihighn: 7.0e-5              # tantala mechanical loss
    Phihighn_slope: 0.1

    ## low index material: silica
    Ylown: 72e9
    Sigmalown: 0.17
    CVlown: 1.6412e6              # Crooks et al, Fejer et al
    Alphalown: 5.1e-7             # Fejer et al
    Betalown: 8e-6                # dn/dT,  (ref. 14)
    ThermalDiffusivitylown: 1.38  # Fejer et al
    Indexlown: 1.45
    Philown: 2.3e-5               # silica mechanical loss
    Philown_slope: 0              # G1600641 and arXiv:1712.05701 suggest
                                  # slopes between 0 and 0.3, depending on
                                  # deposition method. Slawek's analysis in
                                  # 10.1103/PhysRevD.98.122001 assumes zero slope.


  ## Substrate Material parameters
  Substrate:
    Temp: 295
    c2: 7.6e-12                   # Coeff of freq depend. term for bulk mechanical loss, 7.15e-12 for Sup2
    MechanicalLossExponent: 0.77  # Exponent for freq dependence of silica loss, 0.822 for Sup2
    Alphas: 5.2e-12               # Surface loss limit (ref. 12)
    MirrorY: 7.27e10              # N/m^2; Youngs modulus (ref. 4)
    MirrorSigma: 0.167            # Kg/m^3; Poisson ratio (ref. 4)
    MassDensity: 2.2e3            # Kg/m^3; (ref. 4)
    MassAlpha: 3.9e-7             # 1/K; thermal expansion coeff. (ref. 4)
    MassCM: 739                   # J/Kg/K; specific heat (ref. 4)
    MassKappa: 1.38               # J/m/s/K; thermal conductivity (ref. 4)
    RefractiveIndex: 1.45         # mevans 25 Apr 2008
    dndT: 9.6e-6                  # 1/K; Heraeus Suprasil UVL


Laser:
  Wavelength: 1.064e-6            # m
  Power: 165                      # W


Optics:
  Type: 'SignalRecycled'
  Quadrature:
    dc: 1.5707963                 # pi/2 # demod/detection/homodyne phase
  PhotoDetectorEfficiency: 0.96   # photo-detector quantum efficiency


  Loss: 20e-6                     # average per mirror power loss
  BSLoss: 0.5e-3                  # power loss near beamsplitter
  coupling: 1.0                   # mismatch btwn arms & SRC modes; used to
                                  # calculate an effective r_srm
  SubstrateAbsorption: 0.5e-4     # 1/m; 1/m; 0.3 ppm/cm for Hereaus
  pcrit: 10                       # W; tolerable heating power (factor 1 ATC)

  ITM:
    Transmittance: 0.014
    CoatingThicknessLown: 0.308
    CoatingThicknessCap: 0.5
    CoatingAbsorption: 0.5e-6

  ETM:
    Transmittance: 5e-6
    CoatingThicknessLown: 0.27
    CoatingThicknessCap: 0.5

  PRM:
    Transmittance: 0.03

  SRM:
    Transmittance: 0.02
    Tunephase: 0.0                # SEC tuning
    CavityLength: 20              # m, ITM to SRM distance

  Curvature:
    ITM: 30000                    # ROC of ITM
    ETM: 30000                    # ROC of ETM


Squeezer:
  # Define the squeezing you want:
  #   None = ignore the squeezer settings
  #   Freq Independent = nothing special (no filter cavities)
  #   Freq Dependent = applies the specified filter cavities
  #   Optimal = find the best squeeze angle, assuming no output filtering
  #   OptimalOptimal = optimal squeeze angle, assuming optimal readout phase
  Type: 'Freq Dependent'
  AmplitudedB: 15                  # SQZ amplitude [dB]
  InjectionLoss: 0.02             # power loss to sqz
  SQZAngle: 0                     # SQZ phase [radians]
  LOAngleRMS: 10e-3               # quadrature noise [radians]

  # Parameters for frequency dependent squeezing
  FilterCavity:
    fdetune: -5.17    # detuning [Hz]
    L: 4000           # cavity length [m]
    Ti: 1.74e-3       # input mirror transmission [Power]
    Te: 5e-6          # end mirror transmission
    Lrt: 150e-6       # round-trip loss in the cavity
    Rot: 0            # phase rotation after cavity
